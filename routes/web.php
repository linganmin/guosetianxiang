<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// 展示海报
Route::any('poster/{id}', 'ApplyController@poster')->name('poster');


// 微信授权
Route::group(['middleware' => ['web', 'wechat.oauth']], function () {

    Route::any('/', function () {
        return view('index');
    });

    // 生成海报 //
    Route::any('makeposter', 'ApplyController@makePoster')->name('makeposter');

    // 申请
    Route::any('apply', 'ApplyController@apply')->name('apply');


});

Route::any('showdata', function () {
    $clis = \App\Clientele::paginate(20);

    return view('showdata', compact('clis'));

})->name('showdata');


Route::any('excel', function () {

    Excel::create('statistical-data', function ($excel) {
        $excel->sheet('sheet1', function ($sheet) {
            // 设置excel标题行
            $sheet->appendRow(['ID', '微信名', '微信头像', '他们说', '但是', '我是', '海报上的名字', '名字', '年龄', '性别', '报名城市', '所在城市','公司','职位','手机','创建时间']);

            // 遍历数据库中的信息
            $clis = \App\Clientele::all();
            foreach ($clis as $cli) {
                $sheet->appendRow([
                    $cli->id,
                    $cli->wechatname,
                    $cli->avatar,
                    $cli->they,
                    $cli->myself,
                    ($cli->role == '0') ? '我是吃瓜群众' : ($cli->role == '1' ? '我是讲师' : '我是评委'),
                    $cli->name,
                    $cli->username,
                    $cli->age,
                    ($cli->sex == '0') ? '男' : '女',
                    $cli->city,
                    $cli->location,
                    $cli->company,
                    $cli->position,
                    $cli->phone,
                    $cli->created_at,
                ]);

            }
        });
    })->download('xls');
})->name('excel');

//Auth::routes();
//
//Route::get('/home', 'HomeController@index');
