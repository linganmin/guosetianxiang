<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>第二季【中国好讲师】比赛报名</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <!-- Add your keywords and description here for SEO. -->
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>

    <!-- Don't delete this. It's need1ed for development workflow. -->
    <link rel="stylesheet" href="css/bundle.min.css">
    <link href="//cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">

    <style>
        .cropit-preview-background {
            opacity: .5;
        }
        input.cropit-image-zoom-input {
            position: relative;
        }

        #image-cropper {
            overflow: hidden;
        }
    </style>
</head>

<body style="min-height: 510px;">
<div class="loading-overlay">
    <img src="images/loading.svg">
</div>

<div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide slide-1">
            <div style="width:20rem;height: 21rem;font-weight: lighter;" class="item-image animated"
                 data-ani-name="bounceInDown" data-ani-duration="2s"
                 data-ani-delay="0s"></div>
            <p style="width:20rem;top:30rem;font-weight: lighter;" class="item-text animated"
               data-ani-name="slideInLeft" data-ani-duration="0.5s" data-ani-delay="0.3s">
                青春，燃烧在杯中</p>
            <p style="width:20rem;top:30rem;font-weight: lighter;" class="item-text animated"
               data-ani-name="slideInLeft" data-ani-duration="1s" data-ani-delay="0.3s">
                第二季「中国好讲师」</p>
            <p style="width:20rem;top:30rem;font-weight: lighter;" class="item-text animated"
               data-ani-name="slideInLeft" data-ani-duration="1.5s" data-ani-delay="0.3s">
                战火在即</p>
            <p style="width:20rem;top:30rem;font-weight: lighter;" class="item-text animated"
               data-ani-name="slideInLeft" data-ani-duration="2s" data-ani-delay="0.3s">
                等你加入</p>
            <div class="page1-bottom animated" data-ani-name="bounceInUp" data-ani-duration="2s" data-ani-delay="0.3s">
                <p class="page1-2017"></p>
            </div>
        </div>
        <div class="swiper-slide slide-2">
            <div class="item-image animated" data-ani-name="rollIn" data-ani-duration="1s" data-ani-delay="0s"></div>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="0.2s"
               data-ani-delay="0.3s">
                喜欢葡萄酒</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="0.4s"
               data-ani-delay="0.3s">
                那波光起伏里</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="0.6s"
               data-ani-delay="0.3s">
                2000载时光流转</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="0.8s"
               data-ani-delay="0.3s">
                千万人匠心手作</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="1s"
               data-ani-delay="0.3s">
                别人笑我太疯癫</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="1.2s"
               data-ani-delay="0.3s">
                我笑他人看不穿</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="1.4s"
               data-ani-delay="0.3s">
                微醺显真情</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="1.6s"
               data-ani-delay="0.3s">见真知
                明真理</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="1.8s"
               data-ani-delay="0.3s">
                我的青春啊</p>
            <p class="item-text animated" style="font-weight: lighter;line-height: 2.8rem;" data-ani-duration="2s"
               data-ani-delay="0.3s">
                燃烧在杯中</p>
            <div class="page1-bottom animated" data-ani-name="bounceInUp" data-ani-duration="2s" data-ani-delay="0.3s">
                <p class="page1-2017"></p>
            </div>
        </div>
        <div class="swiper-slide " style="background:url('images/add.png') no-repeat ;background-size:100% 100%;">

        </div>
        <div class="swiper-slide slide-3" style="background-size:100% 100%;">
            <div class="img animated" style="width: 20rem;height: 20rem;left: 3rem;bottom: 12rem;"
                 data-ani-name="flipInX" data-ani-duration="1s" data-ani-delay="0s"></div>
            <div class="font">
                <p style="width:14rem;" class="item-text animated " data-ani-name="flipInY" data-ani-duration="1s"
                   data-ani-delay="0.3s">
                    #他们说#</p>
                <p style="width:14rem;" class="item-text animated blue" data-ani-name="flipInY" data-ani-duration="1s"
                   data-ani-delay="0.3s">
                    明明可以靠脸吃饭</p>
                <p style="width:14rem;" class="item-text animated" data-ani-name="flipInY" data-ani-duration="1s"
                   data-ani-delay="0.3s">
                    #但是#</p>
                <p style="width:14rem;" class="item-text animated blue" data-ani-name="flipInY" data-ani-duration="1s"
                   data-ani-delay="0.3s">
                    我们才华横溢难自抑</p>
                <p style="width:14rem;" class="item-text animated" data-ani-name="flipInY" data-ani-duration="1s"
                   data-ani-delay="0.3s">我是讲师
                    葡萄酒小皮</p>
            </div>
            <div class="page3-bottom animated" data-ani-name="bounceInUp" data-ani-duration="2s" data-ani-delay="0.3s"
                 style="background: url(images/bottom.png) no-repeat;background-size:100% 100%;width:100%;bottom:3rem;min-height:65px;">
                {{--<p class="page3-2017" style="width: 100%;height:4.5rem;margin-left:0;"></p>--}}
                {{--<p class="page3-text"></p>--}}
                {{--<p class="erweima"></p>--}}
            </div>

        </div>
        <div class="swiper-slide slide-4">
            <div class="top animated" data-ani-name="bounceInDown" data-ani-duration="1s" data-ani-delay="0.3s">
                <div class="img"></div>
                <div class="date" style=" margin-right: 1rem;"></div>
            </div>
            <div class="form">
                <form action="{{route('makeposter')}}" method="post" enctype="multipart/form-data">
                    <div class="form-top">
                        <div class="fl form-left" style="width:13rem;">
                            <div class="img"></div>
                            <h1 class="title">我的葡萄酒宣言</h1>
                        </div>

                        <div class="fr image-editor" style="position:relative;">
                            <input type="file" class="cropit-image-input"
                                   style="position:absolute;opacity:0;top:0;left:0;width:100%;z-index:99;height:100%;">
                            <div class="upload fr ">

                            </div>
                            <div class="photo fr" style="height:auto;margin-top:0;background:none;">

                                <img src="" style="width:100%;">
                            </div>
                            <div id="tankuang" style="overflow: hidden;width:100%;">
                                <div class="cropit-preview" style="width: 324px;height:510px;"></div>
                                <div class="tankuang-bottom" style="position: fixed;bottom:0;left:0;width:100%;">
                                    <div class="rotation-btns"
                                         style="text-align: center;position: relative;z-index: 999;background:rgba(0,0,0,0.5);margin-top:20px;">
                                        <span class="rotate-ccw-btn"><i class="fa fa-rotate-left" aria-hidden="true"
                                                                        style="font-size:18px;margin-right:10px;color:#eee;"></i></span>

                                        <span class="rotate-cw-btn"><i class="fa fa-rotate-right" aria-hidden="true"
                                                                       style="font-size:18px;color:#eee;"></i></span>
                                        <input type="range" class="cropit-image-zoom-input"
                                               style="margin:10px auto;display: inline;margin-left: 15px;position: relative;top:6px;">
                                        <button type="button" class="result"
                                                style="position:relative;z-index:999;display: inline;">确定
                                        </button>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                    {{--图片使用js赋值，值为base64--}}
                    <input type="hidden" id="image" name="image">
                    {{--csrf--}}
                    {{csrf_field()}}
                    <p class="text">
                        <span class="fl">#他们说#</span>
                        <input type="text" id="they" name="they" placeholder="八个字以内"></p>
                    <p class="text">
                        <span class="fl">#但是#</span>
                        <input type="text" id="myself" name="myself" placeholder="八个字以内"></p>
                    <p class="select">
                        <span style="width: 5rem;margin-right:0;">
                            <input type="radio" id="role1" name="role" value="1">
                            <label for="role1">我是讲师</label>
                        </span>
                        <span style="width: 5rem;margin-right:0;">
                            <input type="radio" id="role2" name="role" value="2">
                            <label for="role2">我是评委</label>
                        </span>
                        <span style="margin-right:0;width: 5rem;">
                            <input type="radio" id="role3" name="role" value="0" checked>
                            <label for="role3">我是吃瓜群众</label>
                        </span>
                    </p>
                    <p class="text name">
                        <span class="fl">我叫</span>
                        <input id="name" type="text" name="name" placeholder="五个字以内"></p>
                    {{--<button type="submit"  class="generate" style="width:15rem;">生成我的专属海报</button>--}}
                    <button type="button" onclick="check()" class="generate" style="width:15rem;">生成我的专属海报</button>
                </form>
            </div>
        </div>
    </div>
</div>


{{--弹出层--}}
<div  id="notice" style="width:100%;height:100%;z-index: 99;position: fixed;left:0;top:0;background: rgba(0,0,0,0.8);display: none;">
    <p style="color: #fff;font-size:18px;text-align: center;width: 100%;position: absolute;top:48%;">海报正在制作中...</p>
</div>
{{--弹出层结束--}}

<button class="up-arrow">
    <i class="icon-angle-double-up"></i>
</button>

<button class="btn-music">
    <i class="icon-note"></i>
</button>
<audio id="background" loop>
    <source src="http://saboran.oss-cn-hangzhou.aliyuncs.com/background.mp3" type="audio/mpeg"/>
</audio>

<!-- Don't delete this. It's needed for development workflow. -->
<script src="js/bundle.min.js"></script>
<script src="js/jquery.cropit.js"></script>
<script src="js/exif.js"></script>
<script>

    $(function () {
        $('.image-editor').cropit({
            imageBackground: true,
            imageBackgroundBorderWidth: 50,
            exportZoom: 1.1
        });

        // 选择好图片打开截取图片
        $(".cropit-image-input").change(function () {
            $("#tankuang").show();
        });


        // 点击弹出框的确定
        $(".result").click(function () {

            // 判断是否为苹果手机上传的图片
            EXIF.getData($('.cropit-preview-background')[0], function () {
                var orientation = EXIF.getTag(this, 'Orientation');
                // 值为6旋转
                if (orientation == 6) {
                    $('.image-editor').cropit('rotateCW');
                }
            });
            // 截取显示框
            var imageData = $('.image-editor').cropit('export');

            $("#tankuang").hide();
            $(".upload").hide();
            $(".photo").show();
            $(".photo img").attr("src", imageData);
            $('#image').val(imageData);
        });

        // 旋转图片
        $('.rotate-cw-btn').click(function () {
            $('.image-editor').cropit('rotateCW');
        });
        $('.rotate-ccw-btn').click(function () {
            $('.image-editor').cropit('rotateCCW');
        });
    });


    // 表单验证
    function check() {
        var image = $('#image').val();
        var they = $('#they').val();
        var myself = $('#myself').val();

        var role = $('input:radio[name="role"]:checked').val();
        var name = $('#name').val();

        if (image == '') {
            alert('请选择图片！');
            return false;
        }
        if (they == '') {
            alert('他们说内容不能为空！');
            return false;
        } else {
            if (they.length > 8) {
                alert('他们说内容请保证八字之内！');
                return false;
            }
        }
        if (myself == '') {
            alert('但是内容不能为空！');
            return false;
        } else {
            if (myself.length > 8) {
                alert('但是内容请保证八字之内！');
                return false;
            }
        }
        if (name == '') {
            alert('我叫内容不能为空！');
            return false;
        } else {
            if (name.length > 5) {
                alert('我叫内容请保证五字之内！');
                return false;
            }
        }
        $('#notice').css('display','block');
        // ajax
        $.post('{{route('makeposter')}}', {
            'image': image,
            'they': they,
            'myself': myself,
            'role': role,
            'name': name,
            '_token': "{{csrf_token()}}"
        }, function (data) {
            if (data.code == '1') {
                window.location.href = "poster/" + data.data.id;
                {{--location.href = "{{route('poster')}}/" + data.data.id;--}}
            } else {
                alert('服务器错误！')
            }
        }, 'json');


    }

    function audioAutoPlay(id) {
        var audio = document.getElementById(id);
        audio.play();
        document.addEventListener("WeixinJSBridgeReady", function () {
            audio.play();
        }, false);
    }
    audioAutoPlay('background');


</script>

</body>

</html>
