@extends('layouts.default')

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>微信名</th>
            <th>微信头像</th>
            <th>他们说</th>
            <th>但是</th>
            <th>我是</th>
            <th>海报上名字</th>
            <th>名字</th>
            <th>年龄</th>
            <th>性别</th>
            <th>报名城市</th>
            <th>所在城市</th>
            <th>公司</th>
            <th>职位</th>
            <th>手机</th>
            <th>创建时间</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clis as $cli)
            <tr>
                <td>{{$cli->id}}</td>
                <td>{{userTextDecode($cli->wechatname)}}</td>
                <td><img src="{{$cli->avatar}}" alt="" style="width: 100px;"></td>
                <td>{{$cli->they}}</td>
                <td>{{$cli->myself}}</td>
                <td>{{($cli->role == '0') ? '我是吃瓜群众' : (($cli->role == 1) ? '我是讲师' : '我是评委')}}</td>
                <td>{{$cli->name}}</td>
                <td>{{$cli->username}}</td>
                <td>{{$cli->age}}</td>
                <td>{{($cli->sex == '0') ? '男' : '女'}}</td>
                <td>{{$cli->city}}</td>
                <td>{{$cli->location}}</td>
                <td>{{$cli->company}}</td>
                <td>{{$cli->position}}</td>
                <td>{{$cli->phone}}</td>
                <td>{{$cli->created_at}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

    <div class="paging">
        {{$clis->links()}}
    </div>
@endsection