<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>@yield('title', 'Sample App') - 获取数据</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>

<div class="container">

    @include('layouts._header')


    <div class="col-lg-12" style="margin-top: 50px;margin-bottom: 50px;">
        @yield('content')

    </div>


    @include('layouts._footer')
</div>
<script src="js/app.js"></script>
</body>
</html>