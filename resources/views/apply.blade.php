<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>第二季【中国好讲师】比赛报名</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="//cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        *{
            box-sizing：border-box;
        }

        html {
            font-size: 62.5%; /* 10÷16=62.5% */
        }

        .fl {
            float: left;
        }

        .fr {
            float: right;
        }

        body {
            /*min-height: 568px;*/
            min-height: 510px;
            font-family: "Microsoft YaHei", Helvetica, sans-serif;
            background: url(images/bj4.png) no-repeat;
            background-size: 100% 100%;
            width: 100%;
            height: 100vh;
            overflow: hidden;
            margin: 0;
            padding: 0;
            position: relative;
        }

        .top {
            width: 90%;
            margin: auto;
            overflow: hidden;
            zoom: 1;
        }

        .top .img {
            float: left;
            width: 8rem;
            height: 8rem;
            background: url(images/page4-logo.png) no-repeat;
            background-size: 100% 100%;
            margin-top: 2rem;
        }

        .top .date {
            float: right;
            width: 14rem;
            height: 5rem;

            margin-top: 3rem;
            background: url(images/2017.png) no-repeat;
            background-size: 100%;
        }



        form {
            width: 80%;
            margin: auto;
        }

        form .title {
            text-align: center;
            color: #fff;
            font-size: 2rem;
        }

        .age {
            width: 50%;
        }

        .input-text i {
            color: #fff;
        }

        .input-text input {
            width: 62%;
            line-height: 2rem;
            border: none !important;
            background: none;
            position: relative;
            top: 1px;
            float: right;
        }

        .input-text span {
            display: inline-block;
            width: 2rem;
            text-align: center;
        }

        .age input {
            width: 45%;
        }

        .input-two {
            overflow: hidden;
            zoom: 1;
        }

        .input-two p {
            width: 50%;
        }

        .input-two .sex {
            border: none;
            width: 40%;
        }

        .input-two .sex input {
            width: auto;
            float: none;
        }

        .submit {
            display: block;
            width: 12rem;
            background: #fff;
            height: 3rem;
            line-height: 3rem;
            color: #30B4D7;
            border: none;
            text-align: center;
            margin: auto;
            margin-top: 10px;
            font-size: 2rem;

        }

        .form-group {
            border-bottom: #fff solid 1px;
            color: #fff;
            line-height: 30px;
            margin-bottom: 5px;
        }

        .form-group label, .form-group .form-control {
            display: inline-block;

        }

        .form-group label {
            width: 32%;

        }

        .form-group label i {
            margin-right: 5px;
            margin-left: 10px;
        }

        .form-group .form-control {
            width: 68%;
            float: right;
            background: none;
            border: none;
            line-height: 30px;
            box-shadow: none;
            margin: 0;
            padding: 0;
        }

        .input-two {
            border-bottom: none;
        }

        .input-two .age {
            width: 50%;
        }

        .input-two .age label {
            width: 42%;
        }

        .input-two .age .form-control {
            width: 58%;
        }

        .input-two .age {
            border-bottom: #fff solid 1px;
        }

        .input-two .sex {
            width: 50%;
        }

        .input-two .sex label {
            width: 45%;
        }

        .input-two .sex i {
            margin: 0;
        }
        .bottom {
            width: 90%;
            margin: auto;
            background: url(images/page4-bottom.png) no-repeat;
            background-size: 100%;
            height: 6.5rem;
            clear: both;
            position: absolute;
            left: 5%;
            padding-top:10px;
            bottom: 5px;
            visibility: visible;
        }
        .form-group img{
            width:20px;
        }
        ::-webkit-input-placeholder{color:#eee;}
        ::-moz-placeholder{color:#eee;}
        :-moz-placeholder{color:#eee;}
        @media screen and (min-width: 320px) {
            body {
                min-height: 568px;
            }
        }
        @media screen and (min-width: 375px) {
            body {
                min-height: 610px;
            }
        }
        /*@media screen and (min-width: 414px) {*/
            /*body {*/
                /*min-height: 736px;*/
            /*}*/
        /*}*/
    </style>
</head>
<body>
<div class="form">
    <div class="top " data-ani-name="bounceInDown" data-ani-duration="1s" data-ani-delay="0.3s">
        <div class="img"></div>
        <div class="date"></div>
    </div>
    {{--<form>--}}
    {{--<h1 class="title">我要成为好讲师</h1>--}}
    {{--<p class="input-text">--}}
    {{--<span><i class="fa fa-user" aria-hidden="true"></i></span>--}}
    {{--姓名<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<div class="input-two">--}}
    {{--<p class="input-text age fl">--}}
    {{--<span><i class="fa fa-birthday-cake" aria-hidden="true"></i></span>--}}
    {{--年龄<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<p class="fr input-text sex">--}}
    {{--<i class="fa fa-odnoklassniki" aria-hidden="true"></i><i class="fa fa-odnoklassniki" aria-hidden="true"></i>--}}
    {{--<input type="radio" name="">男<input type="radio">女--}}

    {{--</p>--}}

    {{--</div>--}}
    {{--<p class="input-text">--}}
    {{--<span><i class="fa fa-user" aria-hidden="true"></i> </span>--}}
    {{--报名城市<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<p class="input-text">--}}
    {{--<span><i class="fa fa-map-marker" aria-hidden="true"></i></span>--}}
    {{--所在城市<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<p class="input-text">--}}
    {{--<span><i class="fa fa-building" aria-hidden="true"></i></span>--}}
    {{--所在公司<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<p class="input-text">--}}
    {{--<span><i class="fa fa-briefcase" aria-hidden="true"></i></span>--}}
    {{--职位<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<p class="input-text">--}}
    {{--<span><i class="fa fa-mobile" aria-hidden="true"></i> </span>--}}
    {{--手机号码<input type="text" name="name" size="12" style="border-color: #878787; border-style: solid; border-top-width: 0px;border-right-width: 0px; border-bottom-width: 1px;border-left-width: 0px" >--}}
    {{--</p>--}}
    {{--<button type="button" class="submit">提交信息</button>--}}
    {{--</form>--}}


    <form role="form" method="post" action="{{route('apply')}}" onsubmit="return check();" style="margin-top: -20px;width:95%;max-width: 300px;">
        <h1 class="title">我要成为好讲师</h1>
        <div class="form-group">
            <label for="username">
                <span style="width: 25px;display: inline-block">
                    {{--<i class="fa fa-user" aria-hidden="true"></i>--}}
                    <img src="images/icon1.png" alt="">
                </span>
                姓名
            </label>
            <input id="username" name="username" type="text" class="form-control">
        </div>

        <div class="form-group input-two">
            <p class="age fl">
                <label for="age">
                    <span style="width: 25px;display: inline-block">
                        {{--<i class="fa fa-birthday-cake" aria-hidden="true"></i>--}}
                        <img src="images/icon2.png" alt="">
                    </span>
                   年龄
                </label>
                <input id="age" name="age" type="number" class="form-control">
            </p>
            <p class="sex fr">
                <label for="sex">
                    {{--<i class="fa fa-odnoklassniki" aria-hidden="true"></i>--}}
                    {{--<i class="fa fa-odnoklassniki" aria-hidden="true"></i>--}}
                    <img src="images/icon3.png" alt="" style="margin-left: 5px;">
                    性别
                </label>
                <input name="sex" value="0" type="radio" checked>男 <input value="1" name="sex" type="radio">女
            </p>
        </div>
        <div class="form-group">
            <label for="city">
                <span style="width: 25px;display: inline-block">
                    {{--<i class="fa fa-user" aria-hidden="true"></i>--}}
                    <img src="images/icon4.png" alt="">
                </span>

                报名城市
            </label>
            <input id="city" name="city" type="text" class="form-control">
        </div>
        <div class="form-group ">
            <label for="location">
                <span style="width: 25px;display: inline-block">
                    {{--<i class="fa fa-map-marker" aria-hidden="true"></i>--}}
                    <img src="images/icon5.png" alt="">
                </span>

                所在城市
            </label>
            <input id="location" name="location" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="company">
                <span style="width: 25px;display: inline-block">
                    {{--<i class="fa fa-building" aria-hidden="true"></i>--}}
                    <img src="images/icon6.png" alt="">
                </span>

                所在公司
            </label>
            <input name="company" id="company" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="position">
                <span style="width: 25px;display: inline-block">
                    {{--<i class="fa fa-briefcase" aria-hidden="true"></i>--}}
                    <img src="images/icon7.png" alt="">
                </span>

                职位
            </label>
            <input id="position" name="position" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="phone">
                <span style="width: 25px;display: inline-block">
                    {{--<i class="fa fa-mobile" aria-hidden="true"></i>--}}
                    <img src="images/icon8.png" alt="">
                </span>

                手机号码
            </label>
            <input id="phone" name="phone" type="number" class="form-control">
        </div>
        <button type="submit" class="submit" style="margin-top: 20px;">提交信息</button>


    </form>
    {{--<div class="bottom animated" data-ani-name="bounceInU" data-ani-duration="1s" data-ani-delay="0.3s"></div>--}}

    <script src="js/jquery.min.js"></script>
    <script>

        function check() {
            var username = $('#username').val();
            var age = $('#age').val();
            var city = $('#city').val();
            var sex = $('#sex').val();
            var location = $('#location').val();
            var company = $('#company').val();
            var position = $('#position').val();
            var phone = $('#phone').val();


            if (username == '') {
                alert('姓名不能为空！');
                return false;
            }
            if (age == '') {
                alert('年龄不能为空！');
                return false;
            }else {
                if(age>100){
                    alert('您输入的年龄不合法！');
                    return false;
                }
            }
            if (city == '') {
                alert('报名城市不能为空！');
                return false;
            }
            if (sex == '') {
                alert('请选择性别！');
                return false;
            }
            if (location == '') {
                alert('所在城市不能为空！');
                return false;
            }
            if (company == '') {
                alert('所在公司不能为空！');
                return false;
            }
            if (position == '') {
                alert('职位不能为空！');
                return false;
            }
            if (phone == '') {
                alert('手机号码不能为空！');
                return false;
            } else {
               if (checkPhone(phone) === 1){
                   return false;
               }
            }


        }

        function checkPhone(phone) {
            if (!(/^1(3|4|5|7|8)\d{9}$/.test(phone))) {
                alert("手机号码有误，请重填!");
                return 1;
            }else {
                return 2;
            }
        }

    </script>

</body>
</html>