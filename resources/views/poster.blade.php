<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>第二季【中国好讲师】比赛报名</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <!-- Add your keywords and description here for SEO. -->
    <meta name="keywords" content="第二届[中国好讲师]">
    <meta name="description" content="第二届[中国好讲师]">
    <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>


    <link rel="stylesheet" href="css/bundle.min.css">
</head>

<body style="background-color: #0A0204;">

<div style='margin:0 auto;display:none;'>
    <img src="images/share-base.png"/>
</div>

<div class="tankuang-main" style="text-align:center;width:95%;background: #fff;margin: 5% auto;padding:5px 0;">
    {{--<img src="{{$poster}}" style="width: 90%;height:500px;margin-top: 10px">--}}
    <div style="width:95%;margin:5px auto;box-shadow: 1px 2px 2px 3px #ccc;">
        <img src="{{$poster}}" style="width: 94%;margin-top: 3%;margin-bottom: 2%">
        <div style="font-size: 12px;color:#333;line-height: 20px;">长按图片保存或发送给好友</div>
    </div>

</div>


<div class="baokan-bottom" style="text-align: center;margin-bottom:2rem;">

    <button style="font-size:1rem;margin-top:0.5rem;height:2rem;line-height:2rem;width:13rem;border-radius:10px;background: none;border:#28bbdd solid 1px; ">
        <a href="{{route('apply')}}" style="color:#28bbdd;text-decoration: none">我要报名参赛</a>
    </button>
    {{--<button style="font-size:1rem;margin-top:0.5rem;height:2rem;line-height:2rem;width:13rem;border-radius:10px;background: none;border:#EEA11E solid 1px; "><a href="{{route('apply')}}" style="color:#EEA11E;text-decoration: none">我要报名参赛</a>--}}
    {{--</button>--}}


</div>
</body>
</html>
