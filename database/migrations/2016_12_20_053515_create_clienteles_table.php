<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clienteles', function (Blueprint $table) {



            // 微信资料
            $table->increments('id');
            $table->string('wechatname')->nullable()->comment('微信名');
            $table->string('avatar')->nullable()->comment('微信头像');
            $table->string('openid',200)->nullable()->comment('微信id');

            // 第一个表单
            $table->string('image')->nullable()->comment('上传的图片');
            $table->string('they')->nullable()->comment('他们说');
            $table->string('myself')->nullable()->comment('我说');
            $table->tinyInteger('role')->default(0)->comment('角色，0：吃瓜群众，1：讲师，2：评委');
            $table->string('name')->nullable()->comment('名字');
            $table->string('poster')->nullable()->comment('生成的海报');

            // 第二个表单
            $table->string('username')->nullable()->comment('名字');
            $table->tinyInteger('age')->nullbale()->default(0)->comment('年龄');
            $table->tinyInteger('sex')->nullable()->default(0)->comment('性别,0为难，1为女');
            $table->string('city')->nullable()->comment('报名城市');
            $table->string('location')->nullable()->comment('所在城市');
            $table->string('company')->nullable()->comment('公司');
            $table->string('position')->nullable()->comment('职位');
            $table->string('phone')->nullable()->comment('手机号');


            $table->timestamps();


            $table->index('openid');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clienteles');
    }
}
