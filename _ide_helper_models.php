<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Clientele
 *
 * @property int $id
 * @property string $wechatname 微信名
 * @property string $avatar 微信头像
 * @property string $openid 微信id
 * @property string $image 上传的图片
 * @property string $they 他们说
 * @property string $myself 我说
 * @property bool $role 角色，0：吃瓜群众，1：讲师，2：评委
 * @property string $name 名字
 * @property string $poster 生成的海报
 * @property string $username 名字
 * @property bool $age 年龄
 * @property bool $sex 性别,0为难，1为女
 * @property string $city 报名城市
 * @property string $location 所在城市
 * @property string $company 公司
 * @property string $position 职位
 * @property string $phone 手机号
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereWechatname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereOpenid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereThey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereMyself($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereRole($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele wherePoster($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereAge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereSex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Clientele whereUpdatedAt($value)
 */
	class Clientele extends \Eloquent {}
}

namespace App{
/**
 * App\Register
 *
 */
	class Register extends \Eloquent {}
}

